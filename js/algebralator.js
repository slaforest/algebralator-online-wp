jQuery(document).ready(function($){
	$("#accordion").accordion({ header: "h3" , heightStyle: "content"});
	$('.nav-button-div').focusin(function () {
		$(this).find('.menu').fadeIn(400);
	});
	$('.nav-button-div').focusout(function(){
		$(this).find('.menu').fadeOut(400);
	});
	//$( "#tabs" ).tabs();

	$( "#clear_button" ).click(function() {
			$( ':input',".function_form").not('.button').val('');
			$('.answer_text').text('');
	});

	$('#equation_select').change(function(){
		var current_type = $(this).val();
		if ('two_points' == current_type) {
			$('#second_point').show();
			$('#slope').hide();
		}else{
			$('#second_point').hide();
			$('#slope').show();
		};
	});

	$( "#submit_button" ).click(function() {
		//Two points 
		if ($(this).attr('name') == 'slope_2_points' || 
			$(this).attr('name') == 'midpoint_2_points' || 
			$(this).attr('name') == 'equation_2_points' ||
			$(this).attr('name') == 'distance_2_points') 
		{
			var x1val = ($('#x1').val().length) ? $('#x1').val() : 0;
			var y1val = ($('#y1').val().length) ? $('#y1').val() : 0;
			var x2val = ($('#x2').val().length) ? $('#x2').val() : 0;
			var y2val = ($('#y2').val().length) ? $('#y2').val() : 0;

			var fullname = $(this).attr('name');//need to regex or splice to get first word
			var answer = fullname.slice(0,fullname.indexOf('_'));
			var data = {
				action: $(this).attr('name'),
				x1 : x1val,
				y1 : y1val,
				x2 : x2val,
				y2 : y2val
			};
			$.post(myAjax.ajaxurl, data, function(response) {
				$('.answer_text').html("The " + answer + " between (" + x1val + ', ' + y1val + ") and ( " + x2val +
					', ' + y2val + ') is <strong>' + response + '</strong>');
			});
			
		}
		//equation of a line
		else if ($(this).attr('name') == 'equation_of_line') {
			var x1val = ($('#x1').val().length) ? $('#x1').val() : 0;
			var y1val = ($('#y1').val().length) ? $('#y1').val() : 0;
			
			
			if ($('#equation_select').val() == 'two_points') {
				var x2val = ($('#x2').val().length) ? $('#x2').val() : 0;
				var y2val = ($('#y2').val().length) ? $('#y2').val() : 0;
				var data = {
					action: $(this).attr('name'),
					x1 : x1val,
					y1 : y1val,
					x2 : x2val,
					y2 : y2val
				};
				var answer = "The equation of a line between (" + x1val + ', ' + y1val + ") and ( " + x2val +
					', ' + y2val + ') is ';

			}else{
				var slope = ($('#slope').val().length) ? $('#slope').val() : 0;
				var data = {
					action: $(this).attr('name'),
					x1 : x1val,
					y1 : y1val,
					slope: slope
				};
				var answer = "The equation of a line passing through (" + x1val + ', ' + y1val + ") with slope of "
				+ slope + " is <br>";
			};
			$.post(myAjax.ajaxurl, data, function(response) {
				$('.answer_text').html(answer + '<strong>' + response + '</strong>');
			});
		}
		//solve quadratic
		else if ($(this).attr('name') == 'solve_quadratic') {
			var a = ($('#A').val().length) ? $('#A').val() : 0;
			var b = ($('#B').val().length) ? $('#B').val() : 0;
			var c = ($('#C').val().length) ? $('#C').val() : 0;
			var answer = "The zeroes of the function " + a + "x<sup>2</sup> + " + b + "x + " + c + " are ";

			var data = {
				action: $(this).attr('name'),
				a: a,
				b: b,
				c: c
			};

			$.post(myAjax.ajaxurl, data, function(response) {
				$('.answer_text').html(answer + '<strong>' + response + '</strong>');
			});
		}

		/*else if ($(this).attr('name') == 'midpoint_2_points') {
			var x1val = ($('#x1').val().length) ? $('#x1').val() : 0;
			var y1val = ($('#y1').val().length) ? $('#y1').val() : 0;
			var x2val = ($('#x2').val().length) ? $('#x2').val() : 0;
			var y2val = ($('#y2').val().length) ? $('#y2').val() : 0;

			var data = {
				action: 'midpoint_2_points',
				x1 : x1val,
				y1 : y1val,
				x2 : x2val,
				y2 : y2val
			};
			$.post(myAjax.ajaxurl, data, function(response) {
				$('.answer_text').html("The midpoint between (" + x1val + ', ' + y1val + ") and ( " + x2val +
					', ' + y2val + ') is <strong>' + response + '</strong>');
					
			});
		};*/
			

			
	});
});

<div class="function_wrapper">
	<h3 class='function_head'><?php echo $title  ?></h3>

	<div class='answer_div'>
		<p class='answer_text'> </p>
	</div>


	<form class="function_form" id="midpoint_two_points_form" method="post" action="" enctype="multipart/form-data">
		<p>Enter in your coordinates for each point below...</p>
		<input type='hidden' name="function_form_submitted" value="Y">
		(<input type='number' id= 'x1' name='x1' placeholder="x1" value='<?php echo $x1?>'> , 
		<input type='number' id= 'y1' name='y1' placeholder="y1" value='<?php echo $y1?>'>)
		<span class="spacing"> </span>
		(<input type='number' id= 'x2' name='x2' placeholder="x2" value='<?php echo $x2?>'> , 
		<input type='number' id= 'y2' name='y2' placeholder="y2" value='<?php echo $y2?>'>)
		<br>
		<input id="submit_button" class="button" type="button" name="midpoint_2_points" value="Calculate" />
		<input id="clear_button" class="button" type="button" value="Clear" />
	</form>


</div>

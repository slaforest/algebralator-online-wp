<?php get_header(); ?>

	<?php


	$args = array(
		'post_type' => 'function',
		'posts_per_page' => -1
	);

	$the_query = new WP_Query( $args );
	
	
	$str_list = array();
	//$tab_label_list = array();

	function add_function_to_array($id, $title, $cat){
			global $str_list;

			if (empty($str_list[$cat->name])) {

		 		$str_list[$cat->name] = '<li><a href="'.get_permalink($id).'?function-title='.$title.'">'.$title.'</a></li>';

		 	}else{
		 		$str_list[$cat->name] .= '<li><a href="'.get_permalink($id).'?function-title='.$title.'">'.$title.'</a></li>';
		 	}

		}

	?>


		<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $categories = get_the_category();
				
				foreach ($categories as $cat) {

				 	add_function_to_array(get_the_ID(), get_the_title(), $cat);
				 } 
				 
			
				endwhile; endif;?>
				
				

<div id="accordion">
	<?php
		foreach ($str_list as $header => $functions) {
			echo "<h3> $header </h3>";
			echo "<div> <ul> $functions </ul> </div>";
		}

	?>
  
</div>

<?php get_footer();?>
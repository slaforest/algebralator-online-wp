<?php
	
	
	require_once(plugin_dir_path( __FILE__ ).'class-algebralator-brain.php');
		
	function theme_styles(){
			wp_register_style( 'main', get_template_directory_uri().'/style.css', __FILE__, false, false);
			wp_enqueue_style('main');
			wp_enqueue_style('algebralator_function', get_template_directory_uri().'/css/algebralator-function.css');
	}
	add_action('wp_enqueue_scripts', 'theme_styles');
	add_action( 'wp_print_scripts', 'theme_styles') ;
	add_theme_support('menus');


	function theme_js(){
		wp_enqueue_script('jquery-ui-accordion');
		wp_enqueue_script('theme_js',get_template_directory_uri().'/js/algebralator.js', array('jquery', 'jquery-ui-tabs'), '', true );
		wp_localize_script( 'theme_js', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
	
	}
	add_action('wp_enqueue_scripts', 'theme_js');

	
	function register_custom_types(){

		register_post_type( "function",
	        array(
	            "labels" => array(
	                "name" => "Functions",
	                "singular_name" => "Function",
	                "add_new" => "New Function",
	                "add_new_item" => "Add New Function",
	                "edit" => "Edit",
	                "edit_item" => "Edit Function",
	                "new_item" => "New Function",
	                "view" => "View",
	                "view_item" => "View Function",
	                "search_items" => "Search Functions",
	                "not_found" => "No Functions found",
	                "not_found_in_trash" => "No Functions found in Trash",
	                "parent" => "Parent Function"
	            ),

	            "public" => true,
	            "menu_position" => 25,
	            "supports" => array( "title", "editor", "comments", "thumbnail" ),
	            "taxonomies" => array( "category" ),
	            //"menu_icon" => plugins_url( "/teacher_press/images/image.png", __FILE__ ),
	            "has_archive" => true
	        )
	    );
	
	}
	add_action( 'init', 'register_custom_types' );

	

	function slope_2_points(){
		global $alg_brain;
		$x1 = $_POST['x1'];
		$y1 = $_POST['y1'];
		$x2 = $_POST['x2'];
		$y2 = $_POST['y2'];
		
		
		echo $alg_brain->slope_between_two_points($x1, $y1, $x2, $y2);
		
		die();
	}
	add_action('wp_ajax_slope_2_points', 'slope_2_points');
	add_action('wp_ajax_nopriv_slope_2_points', 'slope_2_points');

	function midpoint_2_points(){
		global $alg_brain;
		$x1 = $_POST['x1'];
		$y1 = $_POST['y1'];
		$x2 = $_POST['x2'];
		$y2 = $_POST['y2'];
		
		$mp = json_encode($alg_brain->midpoint_between_two_points($x1, $y1, $x2, $y2));
		echo $mp;
		
		die();
	}
	add_action('wp_ajax_midpoint_2_points', 'midpoint_2_points');
	add_action('wp_ajax_nopriv_midpoint_2_points', 'midpoint_2_points');

	function equation_of_line(){
		global $alg_brain;
		$x1 = $_POST['x1'];
		$y1 = $_POST['y1'];
		if (isset($_POST['x2']) && isset($_POST['y2'])) {
			$x2 = $_POST['x2'];
			$y2 = $_POST['y2'];
			$line = $alg_brain->equation_between_two_points($x1, $y1, $x2, $y2);
		}elseif (isset($_POST['slope'])) {
			$m = $_POST['slope'];
			$line = $alg_brain->equation_between_point_slope($x1, $y1, $m);
		}
		
		echo $line;
		
		die();
	}
	add_action('wp_ajax_equation_of_line', 'equation_of_line');
	add_action('wp_ajax_nopriv_equation_of_line', 'equation_of_line');

	function distance_2_points(){
		global $alg_brain;
		$x1 = $_POST['x1'];
		$y1 = $_POST['y1'];
		$x2 = $_POST['x2'];
		$y2 = $_POST['y2'];
		
		$mp = $alg_brain->distance_between_two_points($x1, $y1, $x2, $y2);
		echo $mp;
		
		die();
	}
	add_action('wp_ajax_distance_2_points', 'distance_2_points');
	add_action('wp_ajax_nopriv_distance_2_points', 'distance_2_points');

	function solve_quadratic(){
		global $alg_brain;
		$a = $_POST['a'];
		$b = $_POST['b'];
		$c = $_POST['c'];
		
		
		$z = $alg_brain->solve_quadratic($a, $b, $c);
		var_dump( $z);
		
		die();
	}
	add_action('wp_ajax_solve_quadratic', 'solve_quadratic');
	add_action('wp_ajax_nopriv_solve_quadratic', 'solve_quadratic');
?>
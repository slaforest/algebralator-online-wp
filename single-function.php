<?php
get_header();

if (isset($_GET['function-title'])) {
		$title = $_GET['function-title'];
	}else{
		$title = '';
	}
	if ('Slope Between Two Points' == $title) {
		require(get_template_directory().'/algebralator-views/slope-2-points.php');
	}
	elseif ('Midpoint Between Two Points' == $title) {
		require(get_template_directory().'/algebralator-views/midpoint-2-points.php');
	}
	
	elseif ('Distance Between Two Points' == $title) {
		require(get_template_directory().'/algebralator-views/distance-2-points.php');
	}
	elseif ('Equation of a Line' == $title) {
		require(get_template_directory().'/algebralator-views/equation-of-line.php');
	}
	elseif ('Solve Quadratic' == $title) {
		require(get_template_directory().'/algebralator-views/quadratic.php');
	}
	


?>


<?php get_footer();?>
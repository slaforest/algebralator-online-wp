<?php
	class Class_Algebralator_Brain{
		public function __construct() {
			
			$alg_online_page = array(
						  'post_type'	  => 'page',
						  'post_title'    => 'Algebralator Online',
						  'post_status'   => 'publish');
			//wp_insert_post( $alg_online_page );
			if (!get_page_by_title('Algebralator Online')) {
				wp_insert_post( $alg_online_page );
			}

			$functions_list = array('Linear Functions' => array( 0 => 'Slope Between Two Points',
																 1 => 'Midpoint Between Two Points',
																 2 => 'Equation of a Line',
																 3 => 'Distance Between Two Points'),

								'Quadratics'	   	  => array(  0 => 'Solve Quadratic',
									  							 1 => 'Factor Quadratic',
									  							 2 => 'Complete the Square',
									  							 3 => "Dude, where's my vertex?"),

								'Pythagorean Theorem' => array(  0 => 'Solve Pythagorean Theorem',
									  							 1 => 'Right Triangle Checker'));
			
			$total_functions = $this->count_nested($functions_list);

			$args = array(
				'post_type' => 'function',
				'posts_per_page' => -1
			);

			$the_query = new WP_Query( $args );

			if ($the_query->found_posts != $total_functions) {
				
				foreach ($functions_list as $function_name =>$array) {
					foreach ($array as  $title) {
						$term = get_term_by('name', $function_name, 'category');
						if($term != false ){
						    $id = intval($term->term_id);
						}else{
							$id_array = wp_insert_term($function_name, 'category');
							$id = intval($id_array['term_id']);
						}
						
  						
  						
						$my_post = array(
						  'post_type'	  => 'function',
						  'post_title'    => $title,
						  'post_status'   => 'publish',
						  'post_category' => array($id)
						);
						

						if (!get_page_by_title($title, 'OBJECT', 'function')) {
							$post_id = wp_insert_post( $my_post );
    						wp_set_post_categories( $post_id, array($id) );
						}						
					}
					
				}
			}
		}//end construct

		public function slope_between_two_points($x1, $y1, $x2, $y2){
			$denom = $x2 - $x1;
			if (0 == $denom) {
				return "undefined";
			}else{
				$m = ($y2-$y1)/$denom;
				return $m;
			}

			
		}

		public function midpoint_between_two_points($x1, $y1, $x2, $y2){
			$midpoint_x = ($x1 + $x2)/ 2;
			$midpoint_y = ($y1 + $y2)/ 2;
			return array($midpoint_x, $midpoint_y);

			
		}

		public function equation_between_two_points($x1, $y1, $x2, $y2){
			$m = $this->slope_between_two_points($x1, $y1, $x2, $y2);
			$equation = $this->equation_between_point_slope($x1, $y1, $m);
			return $equation;
			
		}
		public function equation_between_point_slope($x1, $y1, $m){
			$b = $y1 - $m*$x1;
			$equation = sprintf('y = %dx + %d',$m, $b);
			return $equation;
			
		}

		public function distance_between_two_points($x1, $y1, $x2, $y2){
			$d = sqrt(($x1 - $x2)*($x1 - $x2) + ($y1 - $y2)*($y1 - $y2));
			return $d;
					
		}

		public function solve_quadratic($a, $b, $c){
			$a = floatval($a);
			$b = floatval($b);
			$c = floatval($c);
			if (($b*$b - 4*$a*$c) >= 0) {
				$x1 = (-$b + sqrt($b*$b - 4*$a*$c))/(2*$a);
				$x2 = ($b*-1 - sqrt($b*$b - 4*$a*$c))/(2*$a);
				return array($x1, $x2);
			}else{
				return "No real answers.";
			}
			

			
		}
		private function count_nested($arr) {
		    return (count($arr, COUNT_RECURSIVE) - count($arr));
		}
	}//end class_algebralator_brain
	$alg_brain = new Class_Algebralator_Brain();
?>
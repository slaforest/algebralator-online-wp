<!DOCTYPE html>
<html>
	<head>

		<title>
			<?php

				wp_title( '-', true, 'right' );

				bloginfo( 'name' );

			?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale = 1.0">
		<?php wp_head(); ?>
	</head>
	<body>
		<div >
					<hgroup>
						<h1><a href="<?php bloginfo( 'siteurl' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
						<h3><?php bloginfo( 'description' ); ?></h3>
					</hgroup>
				</div>

		
		<nav>
			<?php

				$args = array(
					'menu' => 'main-menu',
					'echo' => false
				);
				echo strip_tags(wp_nav_menu( $args ), '<a>');

			?>
		</nav>

		<div class='nav-button-div'>
			<button class="nav-menu-button">Menu</button>
			<?php echo wp_nav_menu( $args );?>
		</div>
		